#!/usr/bin/env python3
import argparse
import messages_pb2
import requests
import random
import subprocess
import sys
import time
import threading
from timeit import default_timer as timer

from IPython import embed


ARGS = None

# Unique id for this run
RUN_ID = random.randint(0,2**62)

# Event object for sincronization
KEEPALIVE_RUN = threading.Event()
UPDATE_URL = ''


def try_to_post(url, msg, err_msg='Failed to connect to server'):
    '''Try to post data to the url, fails with message'''
    r = requests.post(url, msg, verify='./cert.crt')


def send_keepalive():
    '''Send keepalives to server.'''
    print("beginning keepalive thread")
    
    while KEEPALIVE_RUN.is_set():
        print("sending keepalive")
        try_to_post(UPDATE_URL,
                    messages_pb2.Update(operation=messages_pb2.KEEPALIVE,
                                        run_id=RUN_ID,
                                        req_id=ARGS.req_id).SerializeToString())
        time.sleep(10 + 5*random.random())


def log_command_begin():
    command_line = ' '.join(ARGS.to_be_executed)
    print("Executing %s"%command_line)

    try_to_post(UPDATE_URL,
                messages_pb2.Update(operation=messages_pb2.BEGIN,
                                    command=command_line,
                                    req_id=ARGS.req_id,
                                    run_id=RUN_ID).SerializeToString())

def log_command_end(stdout, stderr, length):
    command_line = ' '.join(ARGS.to_be_executed)
    try_to_post(UPDATE_URL,
                messages_pb2.Update(operation=messages_pb2.FINISH,
                                    command=command_line,
                                    status=process.returncode,
                                    stdout=stdout,
                                    req_id=ARGS.req_id,
                                    run_id=RUN_ID,
                                    stderr=stderr).SerializeToString())


def prepare_arguments():
    global ARGS
    global UPDATE_URL
    parser = argparse.ArgumentParser(description="Notify of job completion")
    parser.add_argument('--proto', dest='proto', default='https',
                        help='Which proto to connect to server')
    parser.add_argument('to_be_executed', metavar='cmd', nargs='+',
                        help='the command that you want to be run')
    parser.add_argument('--req_id', dest='req_id', type=int, required=True,
                        help='The identifier that the bot gave you')
    parser.add_argument('--server', dest='server', default='h4n4.duckdns.org',
                        help='The message broker server address')

    ARGS = parser.parse_args()
    UPDATE_URL = "%s://%s/update"%(ARGS.proto, ARGS.server)
    print("update url: %s"%UPDATE_URL)

if __name__ == "__main__":
    prepare_arguments()

    log_command_begin()

    # Prepare execution
    process = subprocess.Popen(ARGS.to_be_executed,
                               stdout=subprocess.PIPE,
                               stderr=subprocess.PIPE)

    # Start keepalive thread
    KEEPALIVE_RUN.set()
    threading.Thread(target=send_keepalive).start()

    # We are going to have some delay due to the overhead of communite but it
    # should be small for batch jobs
    begin = timer()
    stdout, stderr = [x.decode('utf-8') for x in process.communicate()]
    length = timer() - begin

    # We don't need to send keepalives anymore
    KEEPALIVE_RUN.clear()

    log_command_end(stdout, stderr, length)
